<?xml version="1.0" encoding="utf-8"?>
<section version="5.0"
         xsi:schemaLocation="http://docbook.org/ns/docbook http://docbook.org/xml/5.0/xsd/docbook.xsd"
         xml:id="application_settings.general"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:ns="http://docbook.org/ns/docbook">
  <title>General settings</title>

  <para>
    <informalfigure><screenshot><mediaobject><imageobject>
      <imagedata fileref="images/app_settings/general_settings.png" format="PNG" scale="80" />
    </imageobject></mediaobject></screenshot></informalfigure>     
    This page of the <link linkend="application_settings">application settings dialog</link> lets you change several
    global options that affect the user interface and the graphics system of the program. 
  </para>

  <simplesect>
    <title>User interface</title>
    <para>
      <variablelist>
        <varlistentry>
          <term>Load file: Use alternative file selection dialog</term>
          <listitem>
            <para>This option controls the type of file selector that is shown each time you import a data file into OVITO. 
            By the default, the standard file dialog box of the operating system (OS) is used. If you set this option,
            a built-in dialog is used instead, which may or may not provide some advantages over the OS dialog box.
            </para>
          </listitem>
        </varlistentry>
        <varlistentry>
          <term>Modifiers list: Sort by category</term>
          <listitem>
            <para>If this option is turned on, the <link linkend="particles.modifiers">list of available modifiers</link> 
            will be divided into several categories. If turned off, the modifiers will be shown as
            one contiguous, alphabetically ordered list.  
            </para>
          </listitem>
        </varlistentry>
      </variablelist>
    </para>
  </simplesect>

</section>